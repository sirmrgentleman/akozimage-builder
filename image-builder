#!/bin/sh

#################
# Pretty output #
#################

notice()  { printf "\n\033[0;33m [?] \033[0m$*\n"; step_msg=$*; }
info()    { printf      "\033[0m [!] \033[0m$*\n";              }
fail()    { printf   "\033[0;31m [x] \033[0m$*\n\n"; exit 1;      }
success() { printf   "\033[0;32m [*] \033[0m${step_msg}\n\n";     }

############################
# A few important paths... #
############################

export out=$PWD/out
export res=$PWD/build-resources
steps=$PWD/build-steps

#########
# Flags #
#########

for arg in "$@"; do
    case $arg in
    	-h|--help)
            printf "\nBy default, components are only built if not present yet\n"
            printf "\nUsage:\nimage-builder [options]\n\nOptions:\n"
            echo "-c, --clean        remove output directories"
            echo "-i, --initramfs    force rebuild the initramfs"
            echo "-k, --kernel       force rebuild the kernel"
            echo "-s, --squashfs     force rebuild the squashfs"
            exit 0
        ;;
        -c|--clean)
            clean=true
        ;;
        -i|--initramfs)
            rebuild_initramfs=true
    	;;
        -k|--kernel)
            rebuild_kernel=true
        ;;
        -s|--squashfs)
            rebuild_squashfs=true
        ;;
        *)
            fail "image-builder: invalid option ${arg}\n$(info \
            "Try 'image-builder --help' for more information.")"
        ;;
    esac
done

###########################################
# Ensure this script is running correctly #
###########################################

# Ensure this script is running from the repo root
notice "Checking if running from the correct directory"
[ -f image-builder ] || fail "This script must be run from the repo root!"
success

# Ensure this script is running as root
notice "Checking if root"
[ $(whoami) = "root" ] || fail "This script must be running as root!"
success

######################
# Clean if requested #
######################

if [ "$clean" ]; then
    echo l && rm -rf $out && echo "" && info "Cleaned output!" && exit 0
fi

############################
# Prepare output directory #
############################

rm $out -rf
mkdir -p $out

#######################
# Build the initramfs #
#######################

if [ "$rebuild_initramfs" ]||[ ! -f $steps/initramfs-builder/out/initramfs.cpio ]; then
    notice "Building initramfs..."
    cd $steps/initramfs-builder && ./mkinitramfs && \
    success || fail "Failed to build initramfs"
fi
cp $steps/initramfs-builder/out/initramfs.cpio $out

###############################
# Build the squash filesystem #
###############################

if [ "$rebuild_squashfs" ]||[ ! -f $steps/standard-boot-squashfs/out/root.squashfs ]; then
    notice "Building squashfs..."
    cd $steps/standard-boot-squashfs && ./mkrootfs.sh && \
    success || fail "Failed to build squashfs"
fi
cp $steps/standard-boot-squashfs/out/root.squashfs $out

####################
# Build the kernel #
####################

if [ "$rebuild_kernel" ]||[ ! -f $steps/kernel/out/kernel ]; then
    notice "Building kernel..."
    cd $steps/kernel && ./build-kernel && \
    success || fail "Failed to build kernel"
fi
cp $steps/kernel/out/kernel $out/kernel

########################
# Create the raw image #
########################

# Create a fat filesystem
notice "Creating fat filesystem"
truncate $out/boot.img -s 1024M && \
mkfs -t fat $out/boot.img > /dev/null && \
fatlabel $out/boot.img CORE && \
success || fail "Failed to create fat filesystem"

# Install components to the image
notice "Installing components to image..."
mkdir -p $out/mnt && \
syslinux $out/boot.img && \
mount $out/boot.img $out/mnt && \
cp $out/initramfs.cpio $out/mnt && \
cp $out/root.squashfs $out/mnt && \
cp $out/kernel $out/mnt && \

# Copy configs
cp $res/switch-boot.cfg $out/mnt && \
cp $res/syslinux.cfg $out/mnt && \
success || fail "Failed to install components to image!"

# Unmount the image
notice "Unmounting image"
umount $out/mnt && \
success || fail "Failed to unmount image"
