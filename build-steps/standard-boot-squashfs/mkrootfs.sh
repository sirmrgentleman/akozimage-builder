#!/bin/sh

# Colors
YELLOW="\033[0;33m"
RED="\033[0;31m"
GREEN="\033[0;32m"
PLAIN="\033[0m"

# Message tags
export NOTICE="\n${YELLOW}[?]${PLAIN} "
export INFO="${PLAIN}[!]${PLAIN} "
export FAIL="${RED}[x]${PLAIN} "
export SUCCESS="${GREEN}[*]${PLAIN} "

# Check if running as root
echo -e "${NOTICE}Checking if root"
if [ $(whoami) != "root" ]; then
    echo -e "${INFO}Running as $(whoami)"
    echo -e "${FAIL}This script must be run as root"
    exit 1
fi
echo -e "${SUCCESS}Checking if root"

# Assemble the filesystem
rm out -rf
build-steps/base-fs && \
build-steps/akoziland-fs-essentials && \
build-steps/build-modules

# Make a squashfs from the created filesystem

echo -e "${NOTICE}Creating squashfs"
mksquashfs out/fs out/root.squashfs && \
echo -e "${SUCCESS}Creating squashfs" || \
echo -e "${FAIL}Squashfs was not successfully created"
